
# Canter FullStack Recruitment Task

  The task contains two parts and one bonus task. The main task is divided into frontend and backend tasks.
  This repository contains some help to get started.

##  FRONTEND
    
  Recommended technology: Angular 6-7 (http://www.angular.io)

  Implement a Single Page App (SPA) for listing and manipulating products, including all CRUD functions 
  (crete, read, update and delete). See images for reference.
  
  Views:
  Product list view contains edit and delete buttons and a link to create a new product.
  Edit form for a single product.
  
  ![Form](./images/form.png)
  
  
  ![List](./images/list.png)
  

##  BACKEND

  Recommended technology: Play (https://www.playframework.com/), Scala (https://www.scala-lang.org/)
 
  Implement a REST JSON API with your choice of technologies.
  Data should be persisted (for example database).  

  Example JSON representing a single product:
  
    {
      "id": 32123,
      "name": "CanterBook ÜberPro",
      "category": "laptops",
      "code": "123",
      "price": 12.13,
      "details": [
        {
          "key": "Cpu",
          "value": "16 core, Adeona processor"
        },
        {
          "key": "Display",
          "value": "Yes"
        }
      ]
    }


  REST API specification:

    POST    /products                   // Create new product
    GET     /products                   // List all products
    GET     /products/<id>              // Get one product
    PUT     /products/<id>              // Update product
    DELETE  /products/<id>              // Remove product

## BONUS task

  As a bonus you can implement a navigation search bar (typehead). The search bar should allow the user to type and immediately see 
  the results while they type. When you select one of the results, then you are redirected to corresponding products 
  view (you can reuse create product view or implement a product details view).
  
  REST search API specification:
  
      GET     /products/search/<term>     // Get all products by the search term (either by code or name)
    
  You can use ng-bootstrap typehead compoenent as an example: https://ng-bootstrap.github.io/#/components/typeahead/examples.